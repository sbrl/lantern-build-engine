#!/usr/bin/env bash

version="0.7.1";
LANTERN_VERSION="${version}";

##########################
### Colour Definitions ###
#### ANSI color codes ####
RS="\033[0m"    # reset
HC="\033[1m"    # hicolor
UL="\033[4m"    # underline
INV="\033[7m"   # inverse background and foreground
LC="\033[2m"    # locolor / dim
FBLK="\033[30m" # foreground black
FRED="\033[31m" # foreground red
FGRN="\033[32m" # foreground green
FYEL="\033[33m" # foreground yellow
FBLE="\033[34m" # foreground blue
FMAG="\033[35m" # foreground magenta
FCYN="\033[36m" # foreground cyan
FWHT="\033[37m" # foreground white
BBLK="\033[40m" # background black
BRED="\033[41m" # background red
BGRN="\033[42m" # background green
BYEL="\033[43m" # background yellow
BBLE="\033[44m" # background blue
BMAG="\033[45m" # background magenta
BCYN="\033[46m" # background cyan
BWHT="\033[47m" # background white

URL_START='\e]8;;';
URL_DISPLAY_TEXT='\e\\';
URL_END='\e]8;;\e\\';

CSECTION=${HC}${FBLE};
CTOKEN=${FCYN};
CACTION=${FYEL};
##########################

# Sets the foreground (i.e. text) colour to a custom xterm256 value.
# $1 - The xterm256 number of the colour to set to.
XC() {
	value="${1}";
	echo -en "\e[38;5;${value}m";
}

# Sets the background colour to a custom xterm256 value.
# $1 - The xterm256 colour to set the background colour to.
BXC() {
	value="${1}";
	echo -en "\e[48;5;${value}m";
}

# Sets the title of the terminal window.
# $@ - The new text to set the title to.
set_title() { echo -en '\033]2;'$@'\033\\'; }

# Checks to see if the specified environment variable exists or not.
# @source https://stackoverflow.com/a/35412000/1460422
# $1 - The variable name to check.
is_variable_set() { declare -p $1 &>/dev/null; }

# Display a url in the terminal.
# See https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda
# $1 - The url to link to.
# $2 - The display text to show.
display_url() {
	echo -e "${URL_START}$1${URL_DISPLAY_TEXT}$2${URL_END}";
}

#######################
### Cursor Movement ###
# http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
cursor_position() {
	echo -ne "\033[$1;$2H";
}
cursor_up() {
	echo -ne "\033[$1A";
}
cursor_down() {
	echo -ne "\033[$1B";
}
cursor_right() {
	echo -ne "\033[$1C";
}
cursor_left() {
	echo -ne "\033[$1D";
}
cursor_save() {
	echo -ne "\033[s";
}
cursor_restore() {
	echo -e "\033[u"
}
#######################

terminal_width() {
	# Set default terminal size
	export COLUMNS=80;
	export LINES=25;
	local cols="$(tput cols 2>/dev/null)";
	
	if [[ "$?" -ne 0 ]]; then
		echo "80";
	else
		echo "${cols}";
	fi
}

# $1 - The width of the text to draw
# $2 - The text to draw
right_aligned_text() {
	cursor_save
	echo -ne "\r"; # Reset to the beginning of the line
	location_start=$((( $(terminal_width) - $1))); # Calculate where we need to draw at
	cursor_right ${location_start}; # Jump ahead to the right place
	echo -ne "$2"; # Draw the string
	cursor_restore
}

# $1 - The 2-character status to show
task_status() {
	right_aligned_text 6 "${HC}${FBLE}[${RS} $1 ${HC}${FBLE}]${RS}" >&2;
}

# $1 - The stage name
stage_begin() {
	local term_width=$(terminal_width);
	local title_length=$(echo -ne "$1" | wc -m);
	local padding_length=$((term_width / 2 - title_length / 2 - 2 - 1));
	echo -e "${HC}${FBLE}$(printf '%*s' ${padding_length} | tr ' ' '-')[${RS} $1 ${HC}${FBLE}]$(printf '%*s' ${padding_length} | tr ' ' '-')${RS}" >&2;
	
}

# $1 - The exit code
stage_end() {
	local display_text="${HC}${FGRN}ok${RS}"
	if [[ $1 -ne 0 ]]; then
		display_text="${HC}${FRED}!!${RS}";
	fi
	local term_width=$(terminal_width);
	local padding_length=$((term_width / 2 - 3 - 1));
	echo -e "${HC}${FBLE}$(printf '%*s' ${padding_length} | tr ' ' '-')[${RS} ${display_text} ${HC}${FBLE}]$(printf '%*s' ${padding_length} | tr ' ' '-')${RS}" >&2;
	
	if [[ $1 -ne 0 ]]; then
		exit $1;
	fi
}

# $1 - The task name
task_begin() {
	echo -ne " ${FGRN}*${RS} $1" >&2;
	echo -e "" >&2;
}

# $1 - Exit code
# $2 - Error message (only displayed if the exit code isn't 0)
task_end() {
	cursor_up 1;
	
	if [[ "$1" -ne "0" ]]; then
		echo -ne "\n\n ${FRED}*${RS} $2" >&2;
		task_status "${HC}${FRED}!!${RS}";
		exit $1;
	else
		task_status "${HC}${FGRN}ok${RS}";
	fi
	echo -e "";
}

# $1 - Task name
subtask_begin() {
	echo -ne "    ${FBLE}*${RS} $1" >&2;
}

# $1 - exit code
# $2 - error message (only displayed if the exit code isn't 0)
# $3 - Optional. If set to 'optional', a failure will not cause the build process to terminate.
subtask_end() {
	if [[ "$1" -ne "0" ]]; then
		prefix="\n";
		if [ "$2" != "" ]; then prefix="\r"; fi
		echo -e "${prefix}    ${FRED}*${RS} $2" >&2;
		[ "$3" != "optional" ] && exit $1;
	else
		echo -e "\r    ${FGRN}*${RS}" >&2;
	fi
}

# Echos a command to stdout, and then executes it.
# Usage: execute uname -a
execute() {
	local command_name=$1;
	shift;
	echo -ne "${HC}\$${RS} " >&2;
	echo -n "${command_name}$(printf ' %q' "$@")" >&2;
	echo >&2;
	$command_name "$@";
}

# $1 - Command name to check for
# $2 - Whether to call subtask_begin/end
# $3 - Optional. If set to "optional", then a failure will not terminate the build process.
check_command() {
	if [ "$2" == "true" ]; then
		subtask_begin "Checking for $1";
	fi
	which $1 >/dev/null 2>&1; exit_code=$?
	if [[ "${exit_code}" -ne 0 ]]; then
		subtask_end "${exit_code}" "Error: Couldn't locate $1. Make sure it's installed and in your path." "optional";
		# Handle the optional-ness ourselves to use a specific exit code
		if [ "$3" != "optional" ]; then
			exit 2;
		fi
	fi
	
	if [ "$2" == "true" ]; then
		subtask_end 0;
	fi
}

# Checks to see if the specified environment variable exists.
# $1 - The name of the environment variable to check.
check_env_variable() {
	env_var_name="${1}";
	
	subtask_begin "Checking for environment variable ${env_var_name}";
	is_variable_set "${env_var_name}";
	if [ "${?}" -ne 0 ]; then
		subtask_end 1 "Error: Couldn't find the $1 environment variable. Try checking that's both set correctly and exported (if calling from a script).";
	fi
	subtask_end 0;
}

# Checks to see if a command exists or not.
# $1 - The name of the command to check.
command_exists() {
	command -v $1 >/dev/null 2>&1;
	return "$?";
}


# Runs the specified build tasks in order.
# $@ - the tasks to run.
tasks_run() {
	while test $# -gt 0
	do
		task_$1;
		shift
	done
}

# Originally from https://stackoverflow.com/a/54766117/1460422
# Strips ANSI escape codes from stdin, and writes the output to stdout.
# No arguments.
ansi_strip() {
	shopt -s extglob; # Enable Bash extended globbing expressions
	local line
	local IFS=
	while read -r line || [[ "$line" ]]; do
		echo "${line//$'\e'[\[(]*([0-9;])[@-n]/}";
	done
}
