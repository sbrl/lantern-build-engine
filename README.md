# ![](https://gitlab.com/sbrl/lantern-build-engine/raw/master/logo.png) Lantern Build Engine

> :building\_construction: :construction: A simple bash-based build engine for automating complex, fiddly, and otherwise boring development tasks.

Wow, this ran away from me! This was initially a simple bash script for a private project of mine, but it seems to have turned into a general-purpose engine all by itself. I guess that stealing it for a bunch of University projects had something to do with that.... :P

## Installing
The lantern build engine is meant to be included in your project as a git submodule.

1. Add this repository as a git submodule: `git submodule add {{clone-url}} {{path/to/destination-directory}}`
2. `source` the file `lantern.sh` into your build script.
An example build script can be found in this repository - it's called `build-example`.

## Usage
The lantern build engine basically exposes a set of utility function that make writing a flexible build script _much_ easier. Start by copying the example build script, and customising it to suit your own needs.

Each task is a function that's prefixed with `task_`, so you can wire up task dependencies as complex or as simple as you like! At the end of your build script, execute the provided `tasks_run` function like this to execute all the requested tasks: `tasks_run $@`

### Colour
Lantern provides a number of variables that can be used to quickly colour text output:

Variable    | Colour
------------|-------------------------
`RS`		| Reset
`HC`		| Hi-colour
`UL`		| Underline
`INV`		| Inverse background and foreground
`LC`		| Lo-colour dim
`FBLK`		| Foreground black
`FRED`		| Foreground red
`FGRN`		| Foreground green
`FYEL`		| Foreground yellow
`FBLE`		| Foreground blue
`FMAG`		| Foreground magenta
`FCYN`		| Foreground cyan
`FWHT`		| Foreground white
`BBLK`		| Background black
`BRED`		| Background red
`BGRN`		| Background green
`BYEL`		| Background yellow
`BBLE`		| Background blue
`BMAG`		| Background magenta
`BCYN`		| Background cyan
`BWHT`		| Background white

```bash
echo -e "Here's some ${FYEL}yellow text${RS}!";
```

Additionally, xterm256 colours can be generated with the `XC` and `BXC` functions:

```bash
echo -e "Here's some $(XC 214)orange text${RS}!";
echo -e "Here some text with a $(BXC 205)pink background${RS}!";
```

[This Unix StackExchange answer](https://unix.stackexchange.com/a/124460/64687) might be helpful as a reference to see what the different colours look like.

### `set_title "{{title name}}"`
Sets the title of the terminal the build script is running in. Has no effect if the build script isn't being run in a terminal as far as I'm aware.

```bash
set-title "Current item: ${item_name}";
```

### `display_url "{{url}}" "{{display text}}"`
Displays a url in the terminal. Takes advantage of an [emerging standard](https://gist.github.com/egmontkob/eb114294efbcd5adb1944c9f3cb5feda).

```bash
display_url "https://git.example.com/user/repo/pulls/1234" "Pull Request";
display_url "https://bobsrockets.io/" "Project Website";
```

### `cursor_position {{x}} {{y}}`
Sets the cursor's position on the screen.

```bash
cursor_position 10 20; # Sets the cursor's position to (10, 20)
```

### `cursor_up {{rows}}`
Moves the cursor up by a specified number of rows.

```bash
cursor_up 5;
```

### `cursor_down {{rows}}`
Moves the cursor down by a specified number of rows.

```bash
cursor_down 5;
```

### `cursor_left {{cols}}`
Moves the cursor left by a specified number of columns.

```bash
cursor_left 5;
```

### `cursor_right {{cols}}`
Moves the cursor right by a specified number of columns.

```bash
cursor_right 5;
```

### `cursor_save`
Saves the cursor's current position. Takes no arguments.

```bash
cursor_save;
```

### `cursor_restore`
Restores the cursor's position to the last saved position. Takes no arguments.

```bash
cursor_restore;
```

### `right_aligned_text {{width}} "{{text}}"`
Right-aligns the specified text such that it's on the the right-hand-side of the terminal when `echo`ed. The `{{width}}` parameter here is the width of the provided text, as ANSI colour escape codes throw the width calculations off (a pull request fixing this would be most welcome!).

```bash
right_aligned_text 12 "Hello, World!";
# Example output: "            Hello, World!" (without quotes, of course)
```

### `task_status {{2-char-code}}`
Used internally. Generates a 2-character status code like OpenRC and others do in the terminal on startup. See the example for more details.

```bash
set_status "!!";
# Output: "[ !! ]" (With the square brackets in bright blue)
```

### `stage_begin "{{stage_name}}"`
Begins a build stage. The output will fill the terminal's width.

```bash
stage_begin "Building";
# Output for a 20 column wide terminal:
# ----[ Building ]----
```

### `stage_end {{exit_code}}`
Ends a stage, displaying either `ok` or `!!`, depending on whether the exit code provided is 0 or not (an exit code of 0 means success). The output will be coloured, of course - this isn't displayed below due to limitations in markdown.

```bash
echo "Testing";
stage_end $?;

# Output for a 20 column wide terminal:
# -------[ ok ]-------
```

### `task_begin "{{task_name}}"`
Begins a task. Works similarly to `stage_begin` - the visuals are just different.

```bash
task_begin "Checking environment";
# Output for:
#  * Checking environment
```

### `task_end {{exit_code}}`
The same as `stage_end`, but the visuals are a bit different. The 2-character status code (` [ xy ] `) is displayed at the right-hand side on the line above the line that the cursor is currently on.

```bash
task_begin "Checking environment";
task_end 0;
# Output:
# " * Checking environment          [ ok ] " (without quotes)
```

```bash
task_begin "Combobulating discombobulators";
# ......
task_end $?;
```

...might output something like this:

```
 * Combobulating discombobulators
Output
More output
Even moar output
Yay for output                         [ ok ] 
(cursor position is here)
```

### `subtask_begin "{{task_name}}"`
For splitting up your tasks even further! Generally, if you find yourself using a lot of these, try upgrading to stages and tasks instead of tasks and subtasks - particularly because these don't play so well with output, due to changing the colour of the asterisk (this may be fixed in the future, if the appropriate magic can be conjured :P).

```bash
subtask_begin "Checking git";
# Output: " * \tChecking git" (without quotes, and replacing "\t" with a tab character)
```

### `subtask_end {{exit_code}} [{{error_message}} [optional]]`
The counterpart to `subtask_begin`, this changes the asterisk colour and outputs a 2-character status code to complete a subtask.

```bash
subtask_begin "Checking git";
which git 1>/dev/null 2>&1;
subtask_end $?;
# Output:
#  * \tChecking git            [ ok ]
# (replacing "\t" with a tab character)
```

```bash
subtask_end $? "Error message";
subtask_end $? "Error message" optional;
```

### `execute {{command --with arguments}}`
When a command is prefixed with `execute`, it - along with all its arguments - are first printed to the console before being executed. Useful when executing via continuous-integration. Example:

```bash
execute node --version;
# Output:
# $ node --version
# v11.2.0
```

### `check_command "{{binary_name}}" {{subtask}} [optional]`
Automagically checks for the presence of a command. Kills the process with an exit code of `2` if it can't be found. Setting `{{subtask}}` to `true` causes `subtask_begin` / `subtask_end` to be called appropriately - otherwise it's invisible.

If the 3rd argument is the special keyword `optional`, a fatal error is not thrown.

```bash
task_begin "Checking environment";
check_command "git" true;
check_command "msbuild" true;
check_command "npm" true optional; # Check for npm & write to the terminal, but make it optional
check_command "node"; # Invisibly check for node
check_command "weasyprint" false optional; # Invisibly check for weasyprint, but make it optional
task_end 0;
```

### `command_exists {{command_name}}`
Checks to see if a command exists or not.

 - If it does, then exit code `0` is returned.
 - If it doesn't, then exit code `1` is returned.

```bash
command_exists help
command_exists which
command_exists ufw
command_exists cat
command_exists yayayayayayay
```

### `is_variable_set "{{variable_name}}"`
Checks to see whether an environment variable with the given name is set. Returns an exit code of 0 if the variable exists.

You probably don't want this function. Try looking at `check_env_variable` instead, which is like `check_command` but for environment variables.

```bash
is_variable_set "SOME_VARIABLE_NAME";
echo "Exited with code $?";
```

### `check_env_variable "{{variable_name}}"`
Checks to see if the specified environment variable exists. Throws a fatal error and exits if the environment variable is not found. If this is undesirable, try `is_variable_set` instead, which this function uses under-the-hood.

Basically like `check_command`, but for environment variables.

```bash
task_begin "Checking environment";
check_
task_end 0;
```

### `tasks_run {{task_name}} {{task_name}} .....`
Runs the specified tasks in the specified order. Call this at the bottom of your build script.

```bash
tasks_run setup; # Runs the task_setup function
tasks_run build package; # Runs the build and package tasks as above
tasks_run $@; # Passes all arguments (except the script name / path itself) to the task runner
```

### `ansi_strip`
Originally from [this StackOverflow answer](https://stackoverflow.com/a/54766117/1460422), this utility function takes no arguments and simply strips all ANSI escape codes from stdin, and writes the output to stdout.

Useful for writing lantern output to a target which doesn't support escape codes, such as a log file.

```bash
tasks_run ci | ansi_strip >>"${log_filename}";
tasks_run package | ansi_strip >package.log;
```

## Contributing
Contributions are welcome! Simply fork this repository, make your changes, and submit a Pull Request (aka Merge Request).

All contributions must be declared to have the `Mozilla Public License 2.0` (the same license that this repository is under).

## License
The lantern build engine is licensed under the _Mozilla Public License 2.0_ (MPL-2.0). This license can be found in the _LICENSE_ file in this repository, along with a link to an easy-to-read summary.
